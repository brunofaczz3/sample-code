import {atom} from "recoil";
import {authUser} from "common/auth";

const getDefaultUser = (): authUser | null => {
  const register = localStorage.getItem('army-user');
  const user = register !== null &&
    register !== '' &&
    JSON.parse(register);
  if (!user?.isValidUser) {
    return null;
  }

  return {
    email: user?.email || '',
    password: user?.password || '',
    isValidUser: user?.isValidUser || false
  }
}

export const authAtom = atom<authUser | null>({
  key: 'auth',
  default: getDefaultUser()
});
