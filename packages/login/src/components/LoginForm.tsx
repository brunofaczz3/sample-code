import {Button, Flex, FormControl, FormLabel, Heading, Input} from "@chakra-ui/react";
import React, {useState} from "react";
import useUserActions from "../hooks/useUserActions";

const LoginForm = () => {
  const userActions = useUserActions();

  const [email, setEmail] = useState('')
  const handleEmail = (e: React.FormEvent<HTMLInputElement>) => {
    setEmail(e.currentTarget.value);
  }
  const [password, setPassword] = useState('')
  const handlePassword = (e: React.FormEvent<HTMLInputElement>) => {
    setPassword(e.currentTarget.value);
  }

  const onSubmit = () => {
    userActions.login(email, password);
  }

  return (
    <Flex
      direction='column'
      border='solid 1px lightgray'
      borderRadius={8}
      padding='1rem'
      gap='1rem'
    >
      <Heading margin='auto'>Login</Heading>
      <FormControl>
        <FormLabel htmlFor='email'>Email</FormLabel>
        <Input
          id='email'
          placeholder='Email'
          value={email}
          onChange={handleEmail}
        />
      </FormControl>

      <FormControl>
        <FormLabel htmlFor='password'>Password</FormLabel>
        <Input
          id='password'
          type='password'
          placeholder='Password'
          value={password}
          onChange={handlePassword}
        />
      </FormControl>

      <Button margin='auto' onClick={onSubmit}>Login</Button>
    </Flex>
  )
}

export default LoginForm;
