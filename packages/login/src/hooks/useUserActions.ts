import {useRecoilValue, useSetRecoilState} from 'recoil';
import {authAtom} from "../recoil/atoms";
import {authUser} from "common/auth";
import {useUserActionsResponse} from "auth/hooks";

const useUserActions = (): useUserActionsResponse => {
  const setAuth = useSetRecoilState(authAtom);
  const authValue = useRecoilValue(authAtom);

  const login = (email: string, password: string) => {
    if (email !== 'admin' || password !== 'admin') {
      return;
    }

    const user: authUser = {email, password, isValidUser: true};
    localStorage.setItem('army-user', JSON.stringify(user));
    setAuth(user);
  }

  const logout = () => {
    localStorage.removeItem('army-user');
    setAuth(null);
  }

  return {
    login,
    logout,
    user: authValue
  }
}

export default useUserActions;
