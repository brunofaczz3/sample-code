import App from './App';
import React from 'react';
import {ChakraProvider} from "@chakra-ui/react";
import { createRoot } from 'react-dom/client';

const container = document.getElementById('root');
const root = createRoot(container!);
root.render(
  <ChakraProvider>
    <App />
  </ChakraProvider>,
);
