import App from './App';
import React from 'react';
import {ChakraProvider} from "@chakra-ui/react";
import {createRoot} from 'react-dom/client';
import {RecoilRoot} from "recoil";

const container = document.getElementById('root');
const root = createRoot(container!);
root.render(
  <ChakraProvider>
    <RecoilRoot>
      <App/>
    </RecoilRoot>
  </ChakraProvider>
);
