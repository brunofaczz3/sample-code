import React from 'react';
import {Center, Flex, Heading, Spinner, Button} from '@chakra-ui/react';
import {useUserActions} from 'auth/hooks';
const Login = React.lazy(() => import('auth/Login'));

const App = () => {
  const userActions = useUserActions();

  const handleLogoff = () => {
    userActions.logout();
  }

  if (!userActions.user) {
    return (<>
      <Center
        height="100vh"
        width="100%"
        margin="0"
        p="0"
        flexDirection="column">
        <React.Suspense fallback={<Spinner />}>
          <Login />
        </React.Suspense>
      </Center>
    </>)
  }

  return (
    <>
      <Center
        height="100vh"
        width="100%"
        backgroundColor="#1B1A29"
        margin="0"
        p="0"
        flexDirection="column"
      >
        <Flex
          border="1px solid #151421"
          borderRadius="1rem"
          height="50vh"
          justifyContent="space-around"
          alignItems="center"
          flexDirection="column"
          padding="5rem"
          backgroundColor="#6F60EA"
        >
          <Heading color="#fff">Logged in</Heading>
          <Button as="button" onClick={handleLogoff}>Log off</Button>
        </Flex>
      </Center>
    </>
  )
};

export default App;
