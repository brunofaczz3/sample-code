///<reference types="react" />
///<reference types="./common" />

declare module 'auth/Login' {
  const Login: React.ComponentType;

  export default Login;
}

declare module 'auth/hooks' {
  import {authUser} from "common/auth";

  export interface useUserActionsResponse {
    user: authUser | null;
    login: (email: string, password: string) => void;
    logout: () => void;
  }

  export const useUserActions: () => useUserActionsResponse;
}

