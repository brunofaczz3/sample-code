declare module 'common/auth' {
  export interface authUser {
    email: string;
    password: string;
    isValidUser: boolean;
  }
}
